module Brainfuck where
import System.IO
import Data.Char
import Data.List (intercalate)

data CommandInstruction =
    Inc
  | Dec
  | ShiftLeft
  | ShiftRight
  | In
  | Out
  deriving (Eq, Show)

data Instruction =
    CommandInst CommandInstruction
  | Begin
  | End
  deriving (Eq, Show)

data ProgramFragment =
    Command CommandInstruction
  | Loop Program
  deriving (Eq, Show)

type Program = [ProgramFragment]

data ProcessState = ProcessState {
  leftward  :: [Char],
  cell      :: Char,
  rightward :: [Char],
  input     :: [Char],
  output    :: [Char]
  } deriving Eq


tokenizeChar :: Char -> Maybe Instruction
tokenizeChar '+' = Just $ CommandInst Inc
tokenizeChar '-' = Just $ CommandInst Dec
tokenizeChar '<' = Just $ CommandInst ShiftLeft
tokenizeChar '>' = Just $ CommandInst ShiftRight
tokenizeChar ',' = Just $ CommandInst In
tokenizeChar '.' = Just $ CommandInst Out
tokenizeChar '[' = Just Begin
tokenizeChar ']' = Just End
tokenizeChar _ = Nothing

tokenizeProgram :: [Char] -> [Instruction]
tokenizeProgram [] = []
tokenizeProgram (c:s) = case tokenizeChar c of
  Nothing -> tokenizeProgram s
  Just t -> t:(tokenizeProgram s)

compileTokensRec :: [Instruction] -> Int -> Maybe (Program, [Instruction])
compileTokensRec [] 0 = Just ([], [])
compileTokensRec [] _ = Nothing
compileTokensRec (Begin:rest) n = do
  (p, continuation) <- compileTokensRec rest (n + 1)
  (p', remainder) <- compileTokensRec continuation n
  return ((Loop p):p', remainder)
compileTokensRec (End:rest) 0 = Nothing
compileTokensRec (End:rest) _ = Just ([], rest)
compileTokensRec ((CommandInst i):rest) n = do
  (p, remainder) <- compileTokensRec rest n
  return ((Command i):p, remainder)

compileTokens :: [Instruction] -> Maybe Program
compileTokens p = case compileTokensRec p 0 of
  Just (p, []) -> Just p
  Just (p, _) -> Nothing
  Nothing -> Nothing

compileProgram :: [Char] -> Maybe Program
compileProgram = compileTokens . tokenizeProgram

-- HACK - this will stop working when we start getting input from the outside
-- world, which might actually produce values outside of 0-255
incByte :: Char -> Char
incByte c
  | c < chr 255 = succ c
  | otherwise = chr 0

decByte :: Char -> Char
decByte c
  | c == chr 0 || c > chr 255 = chr 255
  | otherwise = pred c

executeInstruction :: CommandInstruction -> ProcessState -> Maybe ProcessState
executeInstruction Inc s = Just (s { cell = incByte $ cell s })
executeInstruction Dec s = Just (s { cell = decByte $ cell s })
executeInstruction ShiftLeft s = case leftward s of
  [] -> Just $ s { cell = chr 0, rightward = (cell s):(rightward s) }
  next:rest -> Just $ s { leftward = rest, cell = next, rightward = (cell s):(rightward s) }
executeInstruction ShiftRight s = case rightward s of
  [] -> Just $ s { leftward = (cell s):(leftward s), cell = chr 0 }
  next:rest -> Just $ s { leftward = (cell s):(leftward s), cell = next, rightward = rest }
executeInstruction In s = case input s of
  [] -> Nothing
  next:rest -> Just $ s { input = rest, cell = next }
executeInstruction Out s = Just s { output = (cell s):(output s) }

executeProgram :: Program -> ProcessState -> Maybe ProcessState
executeProgram [] s = Just s
executeProgram ((Command c):rest) s = do
  newState <- executeInstruction c s
  result <- executeProgram rest newState
  return result
executeProgram ((Loop p):rest) s =
  if cell s /= chr 0
  then
    do
      newState <- executeProgram p s
      if cell newState == chr 0 then
        executeProgram rest newState
      else
        executeProgram ((Loop p):rest) newState
  else
    executeProgram rest s


executeProgramInCleanEnvironment :: Program -> Maybe ProcessState
executeProgramInCleanEnvironment p = executeProgram p $ ProcessState [] (chr 0) [] [] []

runBrainfuckOnString :: Program -> String -> String
runBrainfuckOnString program input =
  case executeProgram program $ ProcessState input (chr 0) [] [] [] of
    Nothing -> ""
    Just ProcessState { output = o } -> reverse o
