module Main where
import System.Environment
import System.Exit
import System.IO
import Brainfuck

usage = do
  name <- getProgName
  hPutStrLn stderr ("Usage: " ++ name ++ " <program_filename>")
  exitFailure

invalidProgram = hPutStrLn stderr "Compilation failed" >> exitFailure

main :: IO ()
main = do
  args <- getArgs
  if length args /= 1 then usage
  else do
    let filename = args !! 0
    programText <- readFile filename
    case compileProgram programText of
      Nothing -> invalidProgram
      Just program -> interact (runBrainfuckOnString program)
